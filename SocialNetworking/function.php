<?php

	$file = require_once('DBLogin.php');
 
 //setting up the connection with database
 	$connection = new mysqli($dbhost,$dbuser, $dbpass, $dbname);
 	
 	if($connection->connect_error) die($connection->connect_error);
 	//else echo "connected";
 	
 	//create table in database
 	function createTable($name, $query){
 		queryMysql("create table if not exists $name($query)");
 		echo "Table $name created or already exists.<br>";
 	}
 	
 	function queryMysql($query){
 		//to access $connection variable
 		global $connection;
 		$result = $connection->query($query);
 		if(!result) die($connection->error);
 		return $result;
 	}

//destroy session
 	function destroySession(){
 		/*why are we creating this array?*/
 		$_SESSION = array();

        //asking if session_id exist or cookie is set if yes then destroy cookie and session
 		if(session_id()!= "" || isset($_COOKIE[session_name()]))
 			setcookie(session_name(), '',time()-2592000,'/');
 		
 		session_destroy();
 	}

    //sanitization
 	function sanitizeString($var){
 		global $connection;
 		$var = strip_tags($var);
        //strips out all html markup codes and replaces them with a form that displays the characters
        //but doesnt allow a browser to act on them see page 260.
 		$var = htmlentities($var);
 		$var = stripslashes($var);
 		//whats this?
 		return $connection->real_escape_string($var);
 	}


//user profile image and
	function showProfile($user){

        //if user profile image is available
        if(file_exists("$user.jpg"))
            //get the user image
			echo "<img src='$user.jpg' style='float:left;'/>";

        //Gets the profile text
		$result = queryMysql("SELECT * FROM profiles WHERE user='$user'");

        //if result is available then print the profile text
		if($result->num_rows){
			$row= $result->fetch-array(MYSQLI_ASSOC);
			echo stripslashes($row['text']) . "<br style='clear:left;'><br>";
		}
	}
?>

