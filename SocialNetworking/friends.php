<?php
/**
 * shows user's friends and followers. Interrogates friends table just like the members.php program but only for a single user.
 * It then shows user's mutual friends and followers along with the people he is following
 * All the followers are saved in an array called $followers.
 * All the people being followed are saved in an array called $following.
 */

require_once'header.php';

if(!$loggedin) die();

    if(isset($_GET['view'])) $view = sanitizeString($_GET['view']);

    else $view = $user;

    if($view == $user){
        $name1 = $name2 = "Your";
        $name3 = "You are";
    }

    else{
        $name1 = "<a href='members.php?view=$view'>$view</a>'s";
        $name2 = "$view's";
        $name3 = "$view is";
    }

    echo "<div class='main'>";

    //uncomment this line if you wish the user's profile to show here
    //showProfile($view);

    $followers = array();
    $following = array();

    $result = queryMysql("select * from friends where user = '$view'");
    $num = $result->num_rows;

    /*This for loop fills the followers array with those users who are following this user.*/
    for($j = 0; $j< $num; ++$j){
        $row = $result->fetch_array(MYSQLI_ASSOC);
        $followers[$j] = $row['friend'];
    }

    $result = queryMysql("select * from friends where friend = '$view'");

    /*This for loop fills the following array with those users who are followed by this user.*/
    for($j = 0; $j< $num; ++$j){
        $row = $result->fetch_array(MYSQLI_ASSOC);
        $following[$j] = $row['user'];
    }

    //people who are following the user and are being followed by the user
    //this method extracts all members that are common to both arrays.
    //returns a new array
    $mutual = array_intersect($followers,$following);

    $followers = array_diff($followers,$mutual);
    $following = array_diff($following,$mutual);

    $friends = FALSE;

    if(sizeof($mutual)){
        echo "<span class='subhead'>$name2 mutual friends</span><ul>";

        foreach($mutual as $friend)
            echo "<li><a href='members.php?view=$friend'>$friend</a>";

        echo "</ul>";
        $friends = TRUE;

    }

    if(sizeof($followers)){
        echo "<span class='subhead'>$name2 followers</span><ul>";

        foreach($followers as $friend){
            echo "<li><a href='members.php?view=$friend'>$friend</a>";
        }

        echo "</ul>";
        $friends = TRUE;
    }

    if(sizeof($following)){
        echo "<span class='subhead'>$name3 following</span><ul>";

        foreach($following as $friend)
            echo "<li><a href='members.php?view=$friend'>$friend</a>";

        echo "</ul>";
        $friends = TRUE;
    }

    if(!$friends) echo "You dont have any friends yet.<br><br>";

    echo "<a class='button' href='messages.php?view=$view'>" . "View $name2 messages";
?>

</div><br>
</body>
</html>
