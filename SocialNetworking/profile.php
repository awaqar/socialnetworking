<?php
/**
 * Created by PhpStorm.
 * User: azzumwaqar
 * Date: 27/02/17
 * Time: 15:38
 */
require_once "header.php";

if(!loggedin) die("you're not logged in you mug");

echo "<div class='main'><h3>My Profile</h3>";
/*This checks whether some text was posted to the program. if yes, it is sanitised and whitespaces replaced\
    with a single whitepace. also ensure the user exists in the database and that no attempted hacking can succeed
       before inserting this text into the database where it will become the user's about me details.

       if no text was posted, database is queried to see whether any text already exists in order to prepopulate the
       the text area
   */

if(isset($_POST['text'])){
    //sanitize the input text

    $text = sanitizeString($_POST['text']);

    //replace spaces with one space character
    $text = preg_replace('/\s\s+/',' ',$text);

    if($result->num_rows)
        queryMysql("update profiles set text='$text' where user= '$user'");
    else
        queryMysql("insert into profiles values ('$user','$text')");
}

else{
    if($result->num_rows){
        $row = $result->fetch_array(MYSQLI_ASSOC);
        $text = stripslashes($row['text']);
    }
    else $text = "";
}

$text = stripslashes(preg_replace('/\s\s+/',' ',$text));

if(isset($_FILES['image']['name'])){
    $saveto = "$user.jpg";
    move_uploaded_file($_FILES['image']['tmp_name'],$saveto);
    $typeok = TRUE;

    switch($_FILES['image']['type']):
        case "image/gif": $src = imagecreatefromgif($saveto);break;
        case "image/jpeg":
        case "image/pjpeg": $src = imagecreatefromjpeg($saveto);break;
        case "image/png": $src = imagecreatefrompng($saveto);break;
        default:  $typeok = FALSE; break;
    endswitch;

    if($typeok){
        /**
        getImageSize(filename) - returns information about the given filename, return an array[] with 7 elements
        index 0 and 1 contains width and height of the image.
         */
        list($w,$h) = getimagesize($saveto);
        $max = 100;
        $tw = $w;
        $th = $h;

        if($w>$h && $max < $w){
            $th = $max / $w * $h;
            $tw = $max;
        }
        elseif($h>$w && $max < $h){
            $tw = $max / $h * $w;
            $th = $max;
        }
        elseif($max < $w){
            $tw = $th = $max;
        }

        $tmp = imagecreatetruecolor($tw,$th);

        imagecopyresampled($tmp,$src,0,0,0,0,$tw,$th,$W,$h);

        imageconvolution($tmp,array(array(-1,-1,-1), array(-1,16,-1),array(-1,-1,-1)),8,0);

        imagejpeg($tmp,$saveto);

        //free up memory
        imagedestroy($tmp);
        imagedestroy($src);
    }
}


    showProfile($user);

echo <<<_END
<form method ='post' action = 'profile.php' enctype='multipart/form-data'>
    <h3>Enter or Edit your details and/or upload an image</h3>
    <textarea name = 'text' cols = '50' rows = '3'>$text</textarea><br>

_END;
?>

Image: <input type= "file" name = "image" size="14">
        <input type="submit" value = "Save Profile">
</form></div><br>
</body></html>
