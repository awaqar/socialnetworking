<?php
/**
 * Created by PhpStorm.
 * User: azzumwaqar
 * Date: 27/02/17
 * Time: 14:48
 */

require_once 'header.php';

if(isset($_SESSION['user'])){

    destroySession();
    echo "<div class='main'> You have been logged out. PLease " .
        "<a href='index.php'>click here</a> to refresh the page";
}

else
    echo "<div class='main'><br>" . "You cannot log out because youre not logged in mate!";

?>

<br><br></div>
</body>
</html>