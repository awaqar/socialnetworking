<?php
/**
 * Created by PhpStorm.
 * User: azzumwaqar
 * Date: 06/03/17
 * Time: 11:27
 */

require_once'header.php';

//die - echo and quit together.
if(!$loggedin) die();

    //checks if variable view has any parameter appended to it, if yes sanitize it otherwise assign this user.
    if(isset($_GET['view'])) $view = sanitizeString($_GET['view']);
    else $view = $user;

    //checks if text field is filled
    if(isset($_POST['text'])){
        $text = sanitizeString($_POST['text']);

        //if text field is not empty insert message into messages table
        if($text!=""){
            //pm =0 for public, 1 for private
            $pm = substr(sanitizeString($_POST['pm']),0,1);
            $time = time();
            queryMysql("insert into messages VALUES (NULL ,'$user','$view','$pm','$time','$text')");
        }
    }

     //checks if $view is empty or not
     if($view!="") {
         if ($view == $user) $name1 = $name2 = "Your";
         else {
             $name1 = "<a href='members.php?view=$view'>$view</a>'s";
             $name2 = "$view's";
         }

         echo "<div class='main'><h3>$name1 Messages</h3>";
         showProfile($view);

         //message form
         echo <<<_END
        <form method = 'post' action = 'messages.php?view=$view'>
        Type here to leave a message:<br>
        <textarea name='text' cols='40' rows='3'></textarea>
        Public <input type = 'radio' name='pm' value ='0' checked='checked'>
        Private <input type='radio' name='pm' value='1'>
        <input type='submit' value='Post Message'></form><br>
_END;


         /*if(isset($_GET['erase'])){
            $erase = sanitizeString($_GET['erase']);
            queryMysql("Delete from messages where id='$erase' and recip='$user'");
        }

         $query = "select * from messages where recip='$view' ORDER BY time DESC";
         $result = queryMysql($query);
         $num = $result->num_rows;

         for($i=0; $i<$num; ++$i){
             $row = $result->fetch_array(MYSQLI_ASSOC);

             if($row['pm']==0 || $row['auth']==$user || $row['recip']==$user){
                //print the date
                 echo date('M jS \'y g:ia:', $row['time']);
                 //print the user name
                 echo "<a href='messages.php?view=". $row['auth']. "'>" . $row['auth']. "</a>";

                 //if public message, append 'write' else 'whispered'
                 if($row['pm']==0) echo "wrote: &quot;". $row['message']. "&quot;";
                 else echo "whispered: <span class='whisper'>$quot;". $row['message']. "&quot;</span>";

                 if($row['recip'] == $user) echo "[<a href='messages.php?view=$view" . "&erase=". $row['id'] . "'>erase</a>]";

                 echo "<br>";
             }
         }*/
     }

    //if variable $num is empty that means no messages, display no messages message.
    if(!$num) echo "<br><span class='info'>No Messages Yet</span><br><br>";

    //button to refresh messages
    echo "<br><a class='button' href='messages.php?view=$view'>Refresh Messages</a> ";


?>

</div><br>
</body>
</html>